package com.pdfgeneratorpro.Entity;


public class PdfData {
    private String file;


    public PdfData(String file) {
        this.file = file;
    }
    public PdfData(){

    }

    public String getFile() {
        return file;
    }


    public void setFile(String file) {
        this.file = file;
    }
}
