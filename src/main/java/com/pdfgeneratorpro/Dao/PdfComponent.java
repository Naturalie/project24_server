package com.pdfgeneratorpro.Dao;

import com.pdfgeneratorpro.Entity.Pdf;

public interface PdfComponent {

        void createDocument(Pdf pdf, String fileDestination);
}
