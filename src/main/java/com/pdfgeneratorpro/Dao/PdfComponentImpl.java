package com.pdfgeneratorpro.Dao;

import com.lowagie.text.*;
import com.lowagie.text.Font;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.pdfgeneratorpro.Entity.Pdf;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


@Component
public class PdfComponentImpl implements PdfComponent {
    private static final Logger LOGGER = LoggerFactory.getLogger(PdfComponentImpl.class);

    @Autowired
    PdfDao pdfDao;


    @Override
    public void createDocument(Pdf pdf, String fileDestination){
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(fileDestination));
            document.open();


            BaseFont bf = BaseFont.createFont("Helvetica", "ISO-8859-2", false);                   //Nagłówek
            Font normfont = new Font(bf, 12, Font.NORMAL, new Color(6, 0, 2));
            Font greyfont = new Font(bf, 11 ,Font.NORMAL , new Color(98, 98, 98, 243));
            Paragraph empty = new Paragraph(" ");
            java.text.DecimalFormat textformat = new java.text.DecimalFormat(); //tworzymy obiekt DecimalFormat
            textformat.setMaximumFractionDigits(2); //dla df ustawiamy największą ilość miejsc po przecinku
            textformat.setMinimumFractionDigits(2);
            LocalDate localDate = LocalDate.now();
            Paragraph par1 = new Paragraph("Faktura VAT nr " + DateTimeFormatter.ofPattern("dd / MM / yyy").format(localDate),new
                    Font(Font.HELVETICA, 14, Font.BOLD, new Color(6, 0, 2)));
            par1.setAlignment(Element.ALIGN_CENTER);
            document.add(par1);


            document.add(empty);             //Przerwa
            document.add(empty);


            PdfPTable table = new PdfPTable(4);
            table.setWidths(new int[] {100,60,60,100});
            PdfPCell[] pdftempcells = new PdfPCell[20];
            for(int j=0;j<20;j++){
                pdftempcells[j] = new PdfPCell();
                pdftempcells[j].setBorder(Rectangle.NO_BORDER);
            }
            for(int k=0;k<20;k++){
                if(k==0){
                    pdftempcells[k].addElement(new Phrase("Sprzedawca:",new Font(Font.HELVETICA, 12, Font.BOLD, new Color(6, 0, 2))));
                    pdftempcells[k].setFixedHeight(10);
                    table.addCell(pdftempcells[k]);
                }
                else if(k==3){
                    pdftempcells[k].addElement(new Phrase("Nabywca:",new Font(Font.HELVETICA, 12, Font.BOLD, new Color(6, 0, 2))));
                    table.addCell(pdftempcells[k]);
                }
                else if(k==4){
                    pdftempcells[k].addElement(new Phrase(pdf.getSellerCompany(),normfont));
                    pdftempcells[k].setFixedHeight(10);
                    table.addCell(pdftempcells[k]);
                }
                else if(k==8){
                    pdftempcells[k].addElement(new Phrase(pdf.getSellerName(),normfont));
                    pdftempcells[k].setFixedHeight(10);
                    table.addCell(pdftempcells[k]);
                }
                else if(k==11){
                    pdftempcells[k].addElement(new Phrase(pdf.getBuyerName(),normfont));
                    table.addCell(pdftempcells[k]);
                }
                else if(k==12){
                    pdftempcells[k].addElement(new Phrase(pdf.getSellerAddress(),normfont));
                    table.addCell(pdftempcells[k]);
                    pdftempcells[k].setFixedHeight(10);
                }
                else if(k==15){
                    pdftempcells[k].addElement(new Phrase(pdf.getBuyerAddress(),normfont));
                    table.addCell(pdftempcells[k]);
                }
                else if(k==16){
                    pdftempcells[k].addElement(new Phrase(pdf.getSellerPostcode()  + " " + pdf.getSellerCity(),normfont));
                    pdftempcells[k].setFixedHeight(10);
                    table.addCell(pdftempcells[k]);
                }
                else if(k==19){
                    pdftempcells[k].addElement(new Phrase(pdf.getBuyerPostcode() + " " + pdf.getBuyerCity(),normfont));
                    table.addCell(pdftempcells[k]);
                }
                else{
                    pdftempcells[k].addElement(new Phrase(" "));
                    table.addCell(pdftempcells[k]);
                }
            }
            document.add(table);                                                  //Dane kupujacego i sprzedajacego

            document.add(empty);


            PdfPTable table2 = new PdfPTable(4);
            PdfPCell[] pdftempcells2 = new PdfPCell[8];
            float price = 0;
            float fullprice = 0;
            table2.setWidths(new int[] {120,30,50,40});
            for(int j=0;j<8;j++){
                pdftempcells2[j] = new PdfPCell();
                pdftempcells2[j].setBorderColor(Color.gray);
            }
            for(int k=0;k<8;k++){
                if(k==0){
                    pdftempcells2[k].addElement(new Phrase("Opis",new Font(Font.HELVETICA, 12, Font.BOLD, new Color(255, 255, 255, 255))));
                    pdftempcells2[k].setBackgroundColor(Color.DARK_GRAY);
                    pdftempcells2[k].setFixedHeight(35);
                    pdftempcells2[k].setBorderWidth(0.01f);
                    pdftempcells2[k].setBorderWidthLeft(Rectangle.NO_BORDER);
                    pdftempcells2[k].setBorderWidthTop(Rectangle.NO_BORDER);
                    table2.addCell(pdftempcells2[k]);
                }
                else if(k==1){
                    pdftempcells2[k].addElement(new Phrase("Liczba",new Font(Font.HELVETICA, 12, Font.BOLD, new Color(255, 255, 255))));
                    pdftempcells2[k].setBackgroundColor(Color.DARK_GRAY);
                    pdftempcells2[k].setBorderWidth(0.01f);
                    pdftempcells2[k].setBorderWidthTop(Rectangle.NO_BORDER);
                    table2.addCell(pdftempcells2[k]);
                }
                else if(k==2){
                    pdftempcells2[k].addElement(new Phrase("Cena jedn.",new Font(Font.HELVETICA, 12, Font.BOLD, new Color(255, 255, 255))));
                    pdftempcells2[k].setBackgroundColor(Color.DARK_GRAY);
                    pdftempcells2[k].setBorderWidth(0.01f);;
                    pdftempcells2[k].setBorderWidthTop(Rectangle.NO_BORDER);
                    table2.addCell(pdftempcells2[k]);
                }
                else if(k==3){
                    pdftempcells2[k].addElement(new Phrase("Koszt",new Font(Font.HELVETICA, 12, Font.BOLD, new Color(255, 255, 255))));
                    pdftempcells2[k].setBackgroundColor(Color.DARK_GRAY);
                    pdftempcells2[k].setBorderWidth(0.01f);
                    pdftempcells2[k].setBorderWidthRight(Rectangle.NO_BORDER);
                    pdftempcells2[k].setBorderWidthTop(Rectangle.NO_BORDER);
                    table2.addCell(pdftempcells2[k]);
                }
                else if(k==4){
                    pdftempcells2[k].addElement(new Phrase(pdf.getDescription(),greyfont));
                    pdftempcells2[k].setBorderWidth(0.01f);
                    pdftempcells2[k].setBorderWidthBottom(1);
                    pdftempcells2[k].setBorderColorBottom(Color.BLACK);
                    pdftempcells2[k].setFixedHeight(25);
                    pdftempcells2[k].setBorderWidthLeft(Rectangle.NO_BORDER);
                    table2.addCell(pdftempcells2[k]);

                }
                else if(k==5){

                    pdftempcells2[k].addElement(new Phrase(String.valueOf(pdf.getQuantity()),greyfont));
                    pdftempcells2[k].setBorderWidth(0.01f);
                    pdftempcells2[k].setBorderWidthBottom(1);
                    pdftempcells2[k].setBorderColorBottom(Color.BLACK);
                    table2.addCell(pdftempcells2[k]);

                }
                else if(k==6){
                    pdftempcells2[k].addElement(new Phrase(String.valueOf(textformat.format(pdf.getNetPrice())) + " zł",greyfont));
                    pdftempcells2[k].setBorderWidth(0.01f);
                    pdftempcells2[k].setBorderWidthBottom(1);
                    pdftempcells2[k].setBorderColorBottom(Color.BLACK);
                    table2.addCell(pdftempcells2[k]);

                }
                else if(k==7){
                    price = pdf.getNetPrice() * pdf.getQuantity();
                    pdftempcells2[k].addElement(new Phrase(String.valueOf(textformat.format((price*100)/100)) + " zł",greyfont));
                    pdftempcells2[k].setBorderWidth(0.01f);
                    pdftempcells2[k].setBorderWidthBottom(1);
                    pdftempcells2[k].setBorderColorBottom(Color.BLACK);
                    pdftempcells2[k].setBorderWidthRight(Rectangle.NO_BORDER);
                    table2.addCell(pdftempcells2[k]);

                }
            }
            document.add(table2);                                   //pierwsza część tabeli



            PdfPTable table3 = new PdfPTable(4);
            PdfPCell[] pdftempcells3 = new PdfPCell[12];
            table3.setWidths(new int[] {120,30,50,40});
            for(int j=0;j<12;j++){
                pdftempcells3[j] = new PdfPCell();
                pdftempcells3[j].setBorderColor(Color.gray);
            }
            for(int k=0;k<12;k++) {
                if (k == 0) {
                    pdftempcells3[k].addElement(new Phrase(" ",greyfont));
                    pdftempcells3[k].setHorizontalAlignment(70);
                    pdftempcells3[k].setFixedHeight(25);
                    pdftempcells3[k].setBorderWidth(0.01f);
                    pdftempcells3[k].setBorderWidthLeft(Rectangle.NO_BORDER);
                    pdftempcells3[k].setBorderWidthBottom(Rectangle.NO_BORDER);
                    table3.addCell(pdftempcells3[k]);
                } else if (k == 1) {
                    pdftempcells3[k].addElement(new Phrase(" ",greyfont));
                    pdftempcells3[k].setBorderWidth(0.01f);
                    pdftempcells3[k].setBorderWidthBottom(Rectangle.NO_BORDER);
                    table3.addCell(pdftempcells3[k]);
                } else if (k == 2) {
                    pdftempcells3[k].addElement(new Phrase("Netto",greyfont));
                    pdftempcells3[k].setBorderWidth(0.01f);
                    pdftempcells3[k].setBorderWidthBottom(Rectangle.NO_BORDER);
                    table3.addCell(pdftempcells3[k]);
                } else if (k == 3) {
                    pdftempcells3[k].addElement(new Phrase(String.valueOf(textformat.format(price)) + " zł",greyfont));
                    pdftempcells3[k].setBorderWidth(0.01f);
                    pdftempcells3[k].setBorderWidthRight(Rectangle.NO_BORDER);
                    pdftempcells3[k].setBorderWidthBottom(Rectangle.NO_BORDER);
                    table3.addCell(pdftempcells3[k]);
                } else if (k == 4) {
                    pdftempcells3[k].addElement(new Phrase(" ",greyfont));
                    pdftempcells3[k].setFixedHeight(25);
                    pdftempcells3[k].setBorderWidth(0.01f);
                    pdftempcells3[k].setBorderWidthLeft(Rectangle.NO_BORDER);
                    table3.addCell(pdftempcells3[k]);
                } else if (k == 5) {
                    pdftempcells3[k].addElement(new Phrase("Podatek",greyfont));
                    pdftempcells3[k].setBorderWidth(0.01f);
                    table3.addCell(pdftempcells3[k]);
                } else if (k == 6) {
                    pdftempcells3[k].addElement(new Phrase("23,00%",greyfont));
                    pdftempcells3[k].setBorderWidth(0.01f);
                    table3.addCell(pdftempcells3[k]);
                } else if (k == 7) {
                    pdftempcells3[k].addElement(new Phrase(String.valueOf(textformat.format(price*0.23f)) + " zł",greyfont));
                    pdftempcells3[k].setBorderWidth(0.01f);
                    pdftempcells3[k].setBorderWidthRight(Rectangle.NO_BORDER);
                    table3.addCell(pdftempcells3[k]);
                } else if (k == 8) {
                    pdftempcells3[k].addElement(new Phrase(" ",greyfont));
                    pdftempcells3[k].setBorderWidth(0.01f);
                    pdftempcells3[k].setFixedHeight(25);
                    pdftempcells3[k].setBorderWidthBottom(1);
                    pdftempcells3[k].setBorderWidthTop(1);
                    pdftempcells3[k].setBorderColorBottom(Color.BLACK);
                    pdftempcells3[k].setBorderColorTop(Color.BLACK);
                    pdftempcells3[k].setBorderWidthLeft(Rectangle.NO_BORDER);
                    table3.addCell(pdftempcells3[k]);
                } else if (k == 9) {
                    pdftempcells3[k].addElement(new Phrase(" ",greyfont));
                    pdftempcells3[k].setBorderWidth(0.01f);
                    pdftempcells3[k].setBorderWidthBottom(1);
                    pdftempcells3[k].setBorderWidthTop(1);
                    pdftempcells3[k].setBorderColorBottom(Color.BLACK);
                    pdftempcells3[k].setBorderColorTop(Color.BLACK);
                    pdftempcells3[k].setBorderWidthLeft(Rectangle.NO_BORDER);
                    table3.addCell(pdftempcells3[k]);
                } else if (k == 10) {
                    pdftempcells3[k].addElement(new Phrase("Łącznie",greyfont));
                    pdftempcells3[k].setBorderWidth(0.01f);
                    pdftempcells3[k].setBorderWidthBottom(1);
                    pdftempcells3[k].setBorderWidthTop(1);
                    pdftempcells3[k].setBorderColorBottom(Color.BLACK);
                    pdftempcells3[k].setBorderColorTop(Color.BLACK);
                    pdftempcells3[k].setBorderWidthLeft(Rectangle.NO_BORDER);
                    table3.addCell(pdftempcells3[k]);
                } else if (k == 11) {
                    fullprice = price + (price*0.23f);
                    pdftempcells3[k].addElement(new Phrase(String.valueOf(textformat.format(fullprice)) + " zł",greyfont));
                    pdftempcells3[k].setBorderWidth(0.01f);
                    pdftempcells3[k].setBorderWidthBottom(1);
                    pdftempcells3[k].setBorderWidthTop(1);
                    pdftempcells3[k].setBorderColorBottom(Color.BLACK);
                    pdftempcells3[k].setBorderColorTop(Color.BLACK);
                    pdftempcells3[k].setBorderWidthRight(Rectangle.NO_BORDER);
                    pdftempcells3[k].setBorderWidthLeft(Rectangle.NO_BORDER);
                    table3.addCell(pdftempcells3[k]);
                }
            }
            document.add(table3);                                                          //dolna część tabeli (podliczanie kosztów)


            document.add(empty);
            document.add(empty);
            document.add(empty);



            PdfPTable table4 = new PdfPTable(2);
            table4.setWidths(new int[] {10,50});
            PdfPCell[] pdftempcells4 = new PdfPCell[2];
            for(int i=0;i<2;i++){
                pdftempcells4[i] = new PdfPCell();
            }
            pdftempcells4[0].addElement(new Phrase("Do zapłaty: ",new Font(bf, 12, Font.BOLD, new Color(6, 0, 2))));
            pdftempcells4[0].setBorder(Rectangle.NO_BORDER);
            pdftempcells4[1].addElement(new Phrase(String.valueOf(textformat.format(fullprice)) + " zł",normfont));
            pdftempcells4[1].setBorder(Rectangle.NO_BORDER);
            table4.addCell(pdftempcells4[0]);
            table4.addCell(pdftempcells4[1]);
            document.add(table4);                                                        //Podliczona suma do zapłaty liczbowo



            PdfPTable table5 = new PdfPTable(2);
            table5.setWidths(new int[] {7,50});
            PdfPCell[] pdftempcells5 = new PdfPCell[2];
            for(int i=0;i<2;i++){
                pdftempcells5[i] = new PdfPCell();
            }
            pdftempcells5[0].addElement(new Phrase("Słownie: ",new Font(bf, 12, Font.BOLD, new Color(6, 0, 2))));
            pdftempcells5[0].setBorder(Rectangle.NO_BORDER);
            PdfAmountInWords amount = new PdfAmountInWords();

            String kwota = amount.translation(fullprice);
            pdftempcells5[1].addElement(new Phrase(kwota,normfont));
            pdftempcells5[1].setBorder(Rectangle.NO_BORDER);
            table5.addCell(pdftempcells5[0]);
            table5.addCell(pdftempcells5[1]);
            document.add(table5);                                                       //Podliczona suma do zapłaty słownie



            document.close();
            pdfDao.getPdfCollection();
            LOGGER.info("File is successfully saved.");


        }catch (DocumentException | FileNotFoundException e) {
            LOGGER.error("Cannot create document, or file doesn't exist.");
        } catch (IOException e) {
            System.out.println("System doesn't contain that type of font.");
        }
    }


}