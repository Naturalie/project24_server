package com.pdfgeneratorpro.Dao;

import com.pdfgeneratorpro.Entity.Pdf;
import com.pdfgeneratorpro.Entity.PdfData;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

public interface PdfResource {
    Collection<Pdf> getAllPdfs();
    Pdf getPdfById(int id);
    ArrayList<PdfData> getPdfCollection();
    void insertPdfToCollection(PdfData pdfData);


}
