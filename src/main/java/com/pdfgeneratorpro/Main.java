/*
    PdfComponent = DocumentComponent
    PdfComponentImpl = DocumentComponentImpl

    PdfResource =  Resource
    PdfDao = ResourceDao

    Pdf = UserDataDto
    PdfData = FileData

    PdfService = FileServiceImpl
    PdfServiceInter = FileService

    PdfController = PdfGeneratorApiController


 */

package com.pdfgeneratorpro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Main {
    public static void main(String[] args){
        SpringApplication.run(Main.class, args);

    }
}