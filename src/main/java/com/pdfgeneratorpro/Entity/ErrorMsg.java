package com.pdfgeneratorpro.Entity;

import org.springframework.http.HttpStatus;

public enum ErrorMsg {
    ERROR_PATH("Incorrect PATH!", HttpStatus.NOT_FOUND),
            ;

    private String errorMsg;
    private HttpStatus errorCode;

    ErrorMsg(String errorMessage, HttpStatus errorCode) {
        this.errorMsg = errorMessage;
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public HttpStatus getErrorCode() {
        return errorCode;
    }
}
