package com.pdfgeneratorpro.Controller;

import com.pdfgeneratorpro.Entity.ErrorMsg;
import com.pdfgeneratorpro.Entity.NoFDError;
import com.pdfgeneratorpro.Entity.Pdf;
import com.pdfgeneratorpro.Entity.PdfData;
import com.pdfgeneratorpro.Service.PdfService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.nio.file.Files;
import java.util.*;

@RestController
@RequestMapping("/api/invoice")
public class PdfController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PdfController.class);
       private static final String PATH = ".\\pdfs\\";

    @Autowired
    private PdfService pdfService;

    //funkcja testowa servera
    @CrossOrigin
    @RequestMapping(value = "/server-test", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Map<String, String>> serverTest() {

        LOGGER.info("Server is being running.");

        Map<String, String> serverTestMessage = new HashMap<>();
        serverTestMessage.put("server-status", "Everything works, atleast it seems to.");

        return new ResponseEntity<>(serverTestMessage, HttpStatus.OK);
    }

    //funkcja tworząca pdf dla wysłanego JSON'a
    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> createFile(@RequestBody Pdf pdf){

        LOGGER.info("Pdf made for user: {}", pdf.getSellerName());

        final PdfData fileData = pdfService.createFile(pdf, PATH);
        return fileData != null ?
                new ResponseEntity<>(null, HttpStatus.CREATED) :
                new ResponseEntity<>(new Error(ErrorMsg.ERROR_PATH.getErrorMsg()), ErrorMsg.ERROR_PATH.getErrorCode());
    }

    //Funkcja dodajaca custom made pdf.
    @RequestMapping(value = "/pdfAdd", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> insertStudent(@RequestBody PdfData pdfData) {

        final PdfData fileData = pdfService.insertPdf(pdfData);
        return fileData != null ?
                new ResponseEntity<>(fileData, HttpStatus.CREATED) :
                new ResponseEntity<>(new Error("Creating new pdf didn't work out."), HttpStatus.BAD_REQUEST);

    }

    //Funkcja wyswietlajaca wszystkie pdfy w folderze
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getPdfCollection(){


        if((new File(".//pdfs").exists()) == false) {
            return new ResponseEntity<>(new NoFDError("No such directory found, use POST method first."), HttpStatus.NOT_FOUND);
        }

        if(pdfService.getPdfCollection().isEmpty()) {
            return new ResponseEntity<>(new NoFDError("No pdfs detected."), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(pdfService.getPdfCollection(), HttpStatus.OK);
    }

    //Funkcja wywwietlajaca values pdfow.
    @RequestMapping(value = "/pdfCollectAllValues", method = RequestMethod.GET)
    public ResponseEntity<?> getAllPdfs(){

        return new ResponseEntity<> (pdfService.getAllPdfs(), HttpStatus.OK);
    }

    //Funkcja wywwietlajaca values pdfa o danym ID.
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getPdfById(@PathVariable("id") int id){

        return new ResponseEntity<>(pdfService.getPdfById(id), HttpStatus.OK);
    }



}