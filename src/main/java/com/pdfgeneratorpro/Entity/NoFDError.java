package com.pdfgeneratorpro.Entity;

public class NoFDError {
    String errorMsg;

    public NoFDError(String errorMsg){
        this.errorMsg = errorMsg;
    }

    public String getError(){
        return errorMsg;
    }
}
