package com.pdfgeneratorpro.Entity;

public class Error {
    private String errorMsg;

    public Error(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public static String printErrorMsg(String error){ return error;};
}
