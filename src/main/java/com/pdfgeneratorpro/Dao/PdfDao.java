package com.pdfgeneratorpro.Dao;

import com.pdfgeneratorpro.Entity.Pdf;
import com.pdfgeneratorpro.Entity.PdfData;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

@Repository
public class PdfDao implements PdfResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(PdfDao.class);
    private static String PATH = ".\\pdfs\\", temp;
    private static File directory;
    private static Map<Integer, Pdf> pdfs;
    private static ArrayList<PdfData> pdfCollection;
    //przykladowy json
    static {
        pdfs = new HashMap<Integer, Pdf>() {
            {
                put(1, new Pdf(
                        "NoNiezle",
                        "Jan",
                        "Skurwiel",
                        "32-854",
                        "Tarnow",
                        "Tomek",
                        "Tarnow 12",
                        "32-333",
                        "Brzesko",
                        "GeForce",
                        3,
                        400,
                        23));

            }
        };
    }
    static {
        pdfCollection = new ArrayList<PdfData>() {
            {
                add(new PdfData(PATH + "Noniezle.pdf"));

            }
        };
    }


    //wyswietla value pdf znajdujacych sie w serverze
    public Collection<Pdf> getAllPdfs(){
        return this.pdfs.values();
    }

    //tak samo, ale po ID
    public Pdf getPdfById(int id) {
        return this.pdfs.get(id);
    }

    //wyswietla wszystkie pdfy znajdujace sie w folderze ./pdfs
    public ArrayList<PdfData> getPdfCollection(){
        File directory;
        String temp,c;
        PdfData a;
        Boolean b = false;
        directory = new File(PATH);
        File files[] = directory.listFiles();

        for (File f : files) {
            temp = f.getName();
            if (temp.lastIndexOf(".") != -1 && temp.lastIndexOf(".") != 0) {
                if(temp.substring(temp.lastIndexOf(".")).equals(".pdf")) {

                    //aktualizowanie pdfow z folderu
                    a = new PdfData(PATH+temp);
                    for(PdfData data: pdfCollection)
                    {
                        c = data.getFile();
                        if(c.equals(PATH+temp)){
                            b = true;
                            break;
                        }
                    }
                    if(!b)

                        this.pdfCollection.add(new PdfData(PATH+temp));
                }
            }
            else
                System.out.println("null");

            b = false;


        }
            //sprawdza czy zostal usuniety -- usuwa JSONA, ktory jest zdefiniowany programie.
            refreshIfRemoved();
        return this.pdfCollection;
    }
    //refreshuje liste z pdfami
    public void refreshIfRemoved(){
        File temp;
        List<PdfData> toRemove = new ArrayList<PdfData>();
        for(PdfData data : pdfCollection)
        {
            temp = new File(data.getFile());
            if(!temp.exists())
            {
                toRemove.add(data);
            }
        }

        if(!toRemove.isEmpty())
        {
            this.pdfCollection.removeAll(toRemove);
        }

    }

    public void insertPdfToCollection(PdfData pdfData) {
        this.pdfCollection.add(pdfData);
    }

}
