package com.pdfgeneratorpro.Service;

import com.pdfgeneratorpro.Entity.Pdf;
import com.pdfgeneratorpro.Entity.PdfData;

import java.util.ArrayList;
import java.util.Collection;

public interface PdfService{
    PdfData createFile(Pdf pdf, String path);

    Collection<Pdf> getAllPdfs();
    Pdf getPdfById(int id);
    ArrayList<PdfData> getPdfCollection();
    PdfData insertPdf(PdfData pdfData);
}
