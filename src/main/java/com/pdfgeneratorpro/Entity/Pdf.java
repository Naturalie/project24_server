package com.pdfgeneratorpro.Entity;
import java.io.Serializable;
public class Pdf implements Serializable{
    private String sellerCompany, sellerName, sellerAddress, sellerPostcode, sellerCity;
    private String buyerName, buyerAddress, buyerPostcode, buyerCity;
    private String description;
    private int quantity;
    private float netPrice, taxRate = 23;


    public Pdf(String sellerCompany, String sellerName, String sellerAddress, String sellerPostcode, String sellerCity, String buyerName, String buyerAddress, String buyerPostcode, String buyerCity, String description, int quantity, float netPrice, float taxRate) {
        this.sellerCompany = sellerCompany;
        this.sellerName = sellerName;
        this.sellerAddress = sellerAddress;
        this.sellerPostcode = sellerPostcode;
        this.sellerCity = sellerCity;
        this.buyerName = buyerName;
        this.buyerAddress = buyerAddress;
        this.buyerPostcode = buyerPostcode;
        this.buyerCity = buyerCity;
        this.description = description;
        this.quantity = quantity;
        this.netPrice = netPrice;
        this.taxRate = taxRate;
    }
    public Pdf(){

    }

    public String getSellerCompany() {

        return sellerCompany;
    }

    public String getSellerName() {

        return sellerName;
    }

    public String getSellerAddress() {

        return sellerAddress;
    }

    public String getSellerPostcode() {

        return sellerPostcode;
    }

    public String getSellerCity()
    {
        return sellerCity;
    }

    public String getBuyerName() {

        return buyerName;
    }

    public String getBuyerAddress() {

        return buyerAddress;
    }

    public String getBuyerPostcode()
    {
        return buyerPostcode;
    }

    public String getBuyerCity()
    {
        return buyerCity;
    }

    public String getDescription()
    {
        return description;
    }

    public int getQuantity()
    {
        return quantity;
    }

    public float getNetPrice()
    {
        return netPrice;
    }

    public float getTaxRate()
    {
        return taxRate;
    }

    public void setTaxRate(int tax){
        this.taxRate = tax;
    }
}
