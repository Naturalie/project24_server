package com.pdfgeneratorpro.Service;

        import com.pdfgeneratorpro.Dao.PdfComponent;
        import com.pdfgeneratorpro.Dao.PdfDao;
        import com.pdfgeneratorpro.Entity.Pdf;
        import com.pdfgeneratorpro.Entity.PdfData;
        import org.slf4j.Logger;
        import org.slf4j.LoggerFactory;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.stereotype.Service;

        import java.io.IOException;
        import java.nio.file.Files;
        import java.nio.file.Paths;
        import java.time.ZonedDateTime;
        import java.util.ArrayList;
        import java.util.Collection;

@Service
public class PdfServiceImpl implements PdfService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PdfServiceImpl.class);

    @Autowired
    private PdfDao pdfDao;

    @Autowired
    private PdfComponent pdfComponent;

    @Override
    public PdfData createFile(Pdf pdf, String path) {

        String fileName = "invoice_" + ZonedDateTime.now().toInstant().toEpochMilli() + ".pdf";
        String fileDestination = path + fileName;

        try {

            Files.createDirectories(Paths.get(path));
            pdfComponent.createDocument(pdf, fileDestination);

            PdfData fileData = new PdfData(fileName);

            return fileData;

        } catch (IOException e) {
            LOGGER.error("I can't save data.");
        }

        return null;
    }


    public Collection<Pdf> getAllPdfs(){

        return this.pdfDao.getAllPdfs();
    }

    public ArrayList<PdfData> getPdfCollection(){

        return this.pdfDao.getPdfCollection();
    }

    public PdfData insertPdf(PdfData pdfData){

        pdfDao.insertPdfToCollection(pdfData);
        return pdfData;
    }

    public Pdf getPdfById(int id){

        return this.pdfDao.getPdfById(id);
    }
}
